using Godot;

partial class ArmPosition : Marker2D
{
	const float ROTATE_SPEED = 7.0f;
	const float RETURN_TO_NEUTRAL_SPEED = 2.5f;
	const float MAX_ANGLE = 0.6108652381980153f; // 35°

	public Sword? sword = null;

	public override void _Ready()
	{
		base._Ready();
	}

	public void RotateArm(float delta, float direction)
	{
		if (direction != 0)
			this.Rotation = Mathf.MoveToward(
				this.Rotation,
				MAX_ANGLE * Mathf.Sign(direction),
				delta * ROTATE_SPEED);
		else
			this.Rotation = Mathf.MoveToward(this.Rotation, 0.0f, delta * RETURN_TO_NEUTRAL_SPEED);
	}

	public void TakeSword(Sword sword)
	{
		this.sword = sword;
		this.AddChild(this.sword);
		this.sword.IsHeld = true;
		this.sword.Position = new Vector2(0, 0);
		var arm = this.GetNode<Sprite2D>("Arm1");
		arm.Frame = 6;
		arm.ZIndex = -1;
		arm.Position = new Vector2(-1, 0);
		this.sword.Show();
	}

	/// return `null` if no sword was held
	public Sword? ReleaseSword()
	{
		var arm = this.GetNode<Sprite2D>("Arm1");
		if (this.sword is Sword sword_held)
		{
			this.GetNode<CanvasItem>("../Arm2").Show();
			arm.Frame = 27;
			arm.ZIndex = 0;
			arm.Position = new Vector2(0, 0);
			this.RemoveChild(sword_held);
			sword_held.IsHeld = false;
			this.sword = null;
			return sword_held;
		} else {
			return null;
		}
	}
}
