using Godot;

public class LaunchingSword : IPlayerState
{
	public PlayerStateProperties state_properties { get; init; } = new PlayerStateProperties()
	{
		animation = "idle",
		animation_sword = "sword_idle",
		can_move_sword = true,
		can_stop_if_no_movement = false,
		arms_can_move = true,
		sword_positions_per_frame = new (Vector2, float)[] { (new Vector2(4, 0), 0), (new Vector2(4, 0), 0) },
		collision_mode = CollisionMode.STANDING
	};

	public Vector2 GetAcceleration(float delta, Player player)
	{
		var acceleration = new Vector2(0, 0);
		return acceleration;
	}

	public PlayerStateId NextState(Player player)
	{
		return PlayerStateId.IDLE;
	}
}
