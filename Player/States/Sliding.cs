using Godot;

public class PlayerStateSliding : IPlayerState
{
	public PlayerStateProperties state_properties { get; init; } = new PlayerStateProperties
	{
		animation = "sliding",
		animation_sword = null,
		can_move_sword = false,
		arms_can_move = false,
		can_stop_if_no_movement = false,
		sword_positions_per_frame = new (Vector2, float)[] { (new Vector2(-3, 7), 174) },
		collision_mode = CollisionMode.CRAWLING,
	};

	public Vector2 GetAcceleration(float delta, Player player)
	{
		Vector2 acceleration = new Vector2(0, 0);

		acceleration.Y += Physics.GRAVITY * player.weight;
		acceleration -= player.Velocity * Physics.AIR_FRICTION;
		acceleration.X -= player.Velocity.X * player.ground_friction;

		return acceleration;
	}

	public PlayerStateId NextState(Player player)
	{
		var press_down_on_floor = player.IsOnFloor() && (Input.GetActionStrength(player.controls.down) >= 0.5);
		var moving = Mathf.Abs(player.Velocity.X) >= Player.CRAWL_MOVE_THRESHOLD;
		var running = Mathf.Abs(player.Velocity.X) >= Player.RUNNING_THRESHOLD;

		// Determine current state, based on the above variables and the previous state.
		if (!player.IsOnFloor())
		{
			var on_which_wall = player.Velocity.X - player.speed_before_move_and_slide.X;
			if (player.Velocity.Y >= -Player.GRAB_WALL_THRESHOLD && on_which_wall != 0.0f)
				return PlayerStateId.ON_WALL;
			else if (player.Velocity.Y > 0)
				return PlayerStateId.FALLING;
			else
				return PlayerStateId.JUMPING;
		}
		else
		{
			if (press_down_on_floor || !player.CanStand())
			{
				if (!moving)
					return PlayerStateId.SLIDE_CRAWLING;
				else
					return PlayerStateId.SLIDING;
			}
			else
			{
				if (running)
					return PlayerStateId.RUNNING;
				else
					return PlayerStateId.IDLE;
			}
		}
	}
}
