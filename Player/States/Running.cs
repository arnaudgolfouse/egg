using Godot;
using System;

public class PlayerStateRunning : IPlayerState
{
	public PlayerStateProperties state_properties { get; init; } = new PlayerStateProperties()
	{
		animation = "running",
		animation_sword = null,
		can_move_sword = true,
		arms_can_move = false,
		can_stop_if_no_movement = false,
		sword_positions_per_frame = new (Vector2, float)[] { (new Vector2(2, 0), -6), (new Vector2(4, 1), 0), (new Vector2(3, 1), -3) },
		collision_mode = CollisionMode.STANDING
	};

	private const float JUMP_FRICTION = 1.4f;

	public Vector2 GetAcceleration(float delta, Player player)
	{
		var strength_right = Input.GetActionStrength(player.controls.right);
		var strength_left = Input.GetActionStrength(player.controls.left);
		var pressed_jump = 0.0f;
		if (Input.IsActionJustPressed(player.controls.jump))
			pressed_jump = 1.0f;
		var horizontal_movement = strength_right - strength_left;

		var acceleration = Vector2.Zero;

		acceleration.X += player.running_strength * horizontal_movement;
		if (!Input.IsActionPressed(player.controls.down))
			acceleration.Y -= Player.JUMP_STRENGH * pressed_jump / delta;
		acceleration.Y += Physics.GRAVITY * player.weight;
		acceleration -= player.Velocity * Physics.AIR_FRICTION;
		acceleration.X -= player.Velocity.X * player.ground_friction;
		return acceleration;
	}

	public PlayerStateId NextState(Player player)
	{
		var player_is_on_floor = player.IsOnFloor();

		var press_down_on_floor = player_is_on_floor && (Input.GetActionStrength(player.controls.down) >= 0.5);
		var press_jump = Input.IsActionJustPressed(player.controls.jump);
		var moving = Math.Abs(player.Velocity.X) >= Player.CRAWL_MOVE_THRESHOLD;
		var running = Math.Abs(player.Velocity.X) >= Player.RUNNING_THRESHOLD;
		var can_slide = Math.Abs(player.Velocity.X) >= Player.SLIDING_THRESHOLD;

		// Determine current state, based on the above variables and the previous state.
		if (!player_is_on_floor)
		{
			var on_which_wall = player.Velocity.X - player.speed_before_move_and_slide.X;
			if (player.Velocity.Y >= -Player.GRAB_WALL_THRESHOLD && on_which_wall != 0.0)
				return PlayerStateId.ON_WALL;
			else if (player.Velocity.Y > 0)
				return PlayerStateId.FALLING;
			else
			{
				player.Velocity = player.Velocity with { X = player.Velocity.X / JUMP_FRICTION };
				return PlayerStateId.JUMPING;
			}
		}
		else
		{
			if (press_down_on_floor && press_jump)
			{
				if (can_slide)
					return PlayerStateId.SLIDING;
				return PlayerStateId.CRAWLING;
			}
			else if (running)
				return PlayerStateId.RUNNING;
			else
				return PlayerStateId.IDLE;
		}
	}
}
