using Godot;

/// <summary>
/// ID of a <see cref="IPlayerState"/>.
/// </summary>
public enum PlayerStateId
{
	IDLE,
	RUNNING,
	JUMPING,
	FALLING,
	CRAWLING,
	SLIDING,
	SLIDE_CRAWLING,
	ON_WALL,
	ON_WALL_FALLING,
	LAUNCHING_SWORD,
}

enum StateKind
{
	STANDING,
	IN_AIR,
	CRAWLING_OR_SLIDING,
	ON_WALL,
}

public enum CollisionMode
{
	STANDING,
	CRAWLING,
}

public record class PlayerStateProperties
{
	/// <summary>
	/// The default animation for this state.
	/// </summary>
	public required string animation { get; init; }
	/// <summary>
	/// The animation for this state when holding a sword.
	/// </summary>
	public required string? animation_sword { get; init; }
	/// <summary>
	/// Should we pause this animation if no movement is detected ?
	/// </summary>
	public required bool can_stop_if_no_movement { get; init; }
	/// <summary>
	/// Can the player move their arms ?
	/// <para>This also controls whether the arms are shown.</para>
	/// </summary>
	public required bool arms_can_move { get; init; }
	/// <summary>
	/// If the player holds a sword, can it be moved up and down ?
	/// </summary>
	public required bool can_move_sword { get; init; }
	/// <summary>
	/// For each animation frame in <c><see>animation_sword</see></c>, holds
	/// <list type="bullet">
	///  <item>The position of the sword</item>
	///  <item>The base angle of the sword (in radians)</item>
	/// </list>
	/// </summary>
	public required (Vector2, float)[] sword_positions_per_frame { get; init; }
	/// <summary>
	/// Used for setting the collision shape of the player.
	/// </summary>
	public required CollisionMode collision_mode { get; init; }
}


public interface IPlayerState
{
	PlayerStateProperties state_properties { get; init; }

	Vector2 GetAcceleration(float delta, Player player);
	PlayerStateId NextState(Player player);
}
