using Godot;

public class PlayerStateCrawling : IPlayerState
{
	public PlayerStateProperties state_properties { get; init; } = new PlayerStateProperties()
	{
		animation = "crawling",
		animation_sword = null,
		arms_can_move = false,
		can_move_sword = false,
		can_stop_if_no_movement = true,
		sword_positions_per_frame = new (Vector2, float)[] { (new Vector2(-2, 6), 6), (new Vector2(-2, 5), 6) },
		collision_mode = CollisionMode.CRAWLING
	};

	public Vector2 GetAcceleration(float delta, Player player)
	{
		var strength_right = Input.GetActionStrength(player.controls.right);
		var strength_left = Input.GetActionStrength(player.controls.left);
		var strength_down = Input.GetActionStrength(player.controls.down);
		float pressed_jump = Input.IsActionJustPressed(player.controls.jump) ? 1 : 0;
		var horizontal_movement = strength_right - strength_left;

		Vector2 acceleration = new Vector2(0, 0);

		acceleration.X += Player.CRAWLING_STRENGTH * horizontal_movement;
		acceleration.Y += Physics.GRAVITY * player.weight;
		acceleration -= player.Velocity * Physics.AIR_FRICTION;
		acceleration.X -= player.Velocity.X * player.ground_friction;

		return acceleration;
	}

	public PlayerStateId NextState(Player player)
	{
		var press_down_on_floor = player.IsOnFloor() && (Input.GetActionStrength(player.controls.down) >= 0.5);
		var running = Mathf.Abs(player.Velocity.X) >= Player.RUNNING_THRESHOLD;

		// Determine current state, based on the above variables and the previous state.
		if (!player.IsOnFloor())
		{
			var on_which_wall = player.Velocity.X - player.speed_before_move_and_slide.X;
			if (player.Velocity.Y >= -Player.GRAB_WALL_THRESHOLD && on_which_wall != 0.0f)
				return PlayerStateId.ON_WALL;
			else if (player.Velocity.Y > 0)
				return PlayerStateId.FALLING;
			else
				return PlayerStateId.JUMPING;
		}
		else
		{
			player.on_wall_counts = 0;
			if (press_down_on_floor || !player.CanStand())
			{
				return PlayerStateId.CRAWLING;
			}
			else
			{
				if (running)
					return PlayerStateId.RUNNING;
				else
					return PlayerStateId.IDLE;
			}
		}
	}
}
