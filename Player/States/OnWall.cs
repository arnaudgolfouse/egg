using Godot;

public class PlayerStateOnWall : IPlayerState
{
	public PlayerStateProperties state_properties { get; init; } = new PlayerStateProperties()
	{
		animation = "on_wall",
		animation_sword = null,
		can_move_sword = true,
		arms_can_move = false,
		can_stop_if_no_movement = false,
		sword_positions_per_frame = new (Vector2, float)[] { (new Vector2(4, 0.8f), -5) },
		collision_mode = CollisionMode.STANDING
	};

	public Vector2 GetAcceleration(float delta, Player player)
	{
		var strength_right = Input.GetActionStrength(player.controls.right);
		var strength_left = Input.GetActionStrength(player.controls.left);
		var strength_down = Input.GetActionStrength(player.controls.down);
		var horizontal_movement = strength_right - strength_left;

		Vector2 acceleration = new Vector2(0, 0);

		// Slide along the wall if player is our 2nd or more wall jump
		acceleration.Y += (
			Physics.GRAVITY * player.weight *
			(player.on_wall_counts - 1) * Player.WALL_FALL_STRENGTH);
		acceleration.Y += Player.WALL_SLIDE_STRENGTH * strength_down;
		acceleration -= player.Velocity * Physics.AIR_FRICTION;
		acceleration.X -= player.Velocity.X * player.ground_friction;
		acceleration.Y -= player.Velocity.Y * Physics.WALL_FRICTION;

		return acceleration;
	}

	public PlayerStateId NextState(Player player)
	{
		float pressed_jump = Input.IsActionJustPressed(player.controls.jump) ? 1 : 0;
		var running = Mathf.Abs(player.Velocity.X) >= Player.RUNNING_THRESHOLD;

		var old_player_wall_sucking_speed = player.wall_sucking_speed;
		player.wall_sucking_speed = 0;

		// Determine current state, based on the above variables and the previous state.
		if (!player.IsOnFloor())
		{
			if (player.IsOnWall())
			{
				if (pressed_jump > 0)
				{
					player.Velocity += new Vector2(
						Player.JUMP_STRENGH * player.wall_direction * 0.75f,
						-Player.JUMP_STRENGH * pressed_jump
					);
					return PlayerStateId.JUMPING;
				}
				else
				{
					// stay on the wall
					player.wall_sucking_speed = old_player_wall_sucking_speed;
					return PlayerStateId.ON_WALL;
				}
			}
			else
				return PlayerStateId.FALLING;
		}
		else
		{
			// touch ground
			player.on_wall_counts = 0;

			if (running)
				return PlayerStateId.RUNNING;
			else
				return PlayerStateId.IDLE;

		}
	}
}
