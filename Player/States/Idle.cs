using Godot;
using System;

public class PlayerStateIdle : IPlayerState
{
	public PlayerStateProperties state_properties { get; init; } = new PlayerStateProperties()
	{
		animation = "idle",
		animation_sword = "sword_idle",
		can_stop_if_no_movement = false,
		can_move_sword = true,
		arms_can_move = true,
		sword_positions_per_frame = new (Vector2, float)[] { (new Vector2(4, 0), 0), (new Vector2(4, 0), 0) },
		collision_mode = CollisionMode.STANDING
	};

	public Vector2 GetAcceleration(float delta, Player player)
	{
		var strength_right = Input.GetActionStrength(player.controls.right);
		var strength_left = Input.GetActionStrength(player.controls.left);
		var pressed_jump = 0.0f;
		if (Input.IsActionJustPressed(player.controls.jump))
			pressed_jump = 1.0f;
		var horizontal_movement = strength_right - strength_left;

		var acceleration = Vector2.Zero;

		acceleration.X += player.running_strength * horizontal_movement;
		if (!Input.IsActionPressed(player.controls.down))
			acceleration.Y -= Player.JUMP_STRENGH * pressed_jump / delta;
		acceleration.Y += Physics.GRAVITY * player.weight;
		acceleration -= player.Velocity * Physics.AIR_FRICTION;
		acceleration.X -= player.Velocity.X * player.ground_friction;

		return acceleration;
	}

	public PlayerStateId NextState(Player player)
	{
		var player_is_on_floor = player.IsOnFloor();

		var press_down_on_floor = player_is_on_floor && (Input.GetActionStrength(player.controls.down) >= 0.5);
		var press_jump = Input.IsActionJustPressed(player.controls.jump);
		var running = Math.Abs(player.Velocity.X) >= Player.RUNNING_THRESHOLD;

		// Determine current state, based on the above variables and the previous state.
		if (!player_is_on_floor)
		{
			if (player.Velocity.Y > 0)
				return PlayerStateId.FALLING;
			else
				return PlayerStateId.JUMPING;
		}
		else
		{
			if (press_down_on_floor && press_jump)
				return PlayerStateId.CRAWLING;
			else if (running)
				return PlayerStateId.RUNNING;
		}

		return PlayerStateId.IDLE;
	}
}
