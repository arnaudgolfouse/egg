using Godot;
using System.Collections.Generic;

public struct PlayerControls
{
	public string up;
	public string down;
	public string right;
	public string left;
	public string attack;
	public string jump;
}

public partial class Player : CharacterBody2D
{
	public const float RUNNING_THRESHOLD = 20.0f;
	public const float SLIDING_THRESHOLD = 30.0f;
	public const float CRAWL_MOVE_THRESHOLD = 4.0f;
	public const float CHANGE_DIRECTION_THRESHOLD = 15.0f;
	public const float GRAB_WALL_THRESHOLD = 10.0f;
	const float BOBBLE_ANIMATION_START = Mathf.Pi / 2.0f;

	/// <summary>
	/// Speed (in frames per second) on the 'idle' animation
	/// </summary>
	const float BOBBLE_SPEED = 3.0f;

	/// <summary>
	/// Initial strength of a jump.
	/// </summary>
	public const float JUMP_STRENGH = 200.0f;
	/// <summary>
	/// Speed multiplier of the voluntary slide against a wall.
	/// </summary>
	public const float WALL_SLIDE_STRENGTH = 1000.0f;
	/// <summary>
	/// Speed multiplier for falling along a wall. 
	/// </summary>
	public const float WALL_FALL_STRENGTH = 1.6f;
	/// <summary>
	/// Running speed multiplier
	/// </summary>
	public const float RUNNING_WITHOUT_SWORD_STRENGTH = 1350.0f;
	/// <summary>
	/// Running speed multiplier with a sword in hand.
	/// </summary>
	public const float RUNNING_WITH_SWORD_STRENGTH = 1200.0f;
	/// <summary>
	/// Running speed multiplier while in the air.
	/// </summary>
	public const float IN_AIR_MOVEMENT_STRENGTH = 1000.0f;
	/// <summary>
	/// Crawling/sliding speed multiplier
	/// </summary>
	public const float CRAWLING_STRENGTH = 190.0f;
	public const float WALL_SUCKING_STRENGTH = 10.0f;
	public const float THROW_SWORD_STRENGTH = 300.0f;

	/// <summary>
	/// Get the kind of the given state.
	/// </summary>
	/// <param name="state">State to get the kind of.</param>
	/// <returns>The kind of <c>state</c></returns>
	static StateKind GetStateKind(PlayerStateId state)
	{
		switch (state)
		{
			case PlayerStateId.IDLE:
			case PlayerStateId.RUNNING:
				return StateKind.STANDING;
			case PlayerStateId.JUMPING:
			case PlayerStateId.FALLING:
				return StateKind.IN_AIR;
			case PlayerStateId.CRAWLING:
			case PlayerStateId.SLIDING:
			case PlayerStateId.SLIDE_CRAWLING:
				return StateKind.CRAWLING_OR_SLIDING;
			case PlayerStateId.ON_WALL:
			case PlayerStateId.ON_WALL_FALLING:
				return StateKind.ON_WALL;
			default:
				return StateKind.STANDING;
		}
	}

	private PackedScene sword_scene = new PackedScene();

	public PlayerControls controls;

	public float weight = 62f;
	/// <summary>
	/// The current running strength
	/// </summary>
	public float running_strength = RUNNING_WITHOUT_SWORD_STRENGTH;
	/// <summary>
	/// Friction when walking
	/// </summary>
	public float ground_friction = Physics.GROUND_FRICTION;
	public Vector2 speed_before_move_and_slide;
	/// <summary>
	/// !!! HACK !!!
	/// <para>Walls sucks us a liiitle bit, so that we properly stick to them.</para>
	/// </summary>
	public float wall_sucking_speed = 0.0f;

	/// <summary>
	/// Number of walls we climbed on since the last time we touched the ground.
	/// </summary>
	public int on_wall_counts = 0;
	/// <summary>
	/// Only used while on a wall.
	/// 1: we face right, -1: we face left
	/// </summary>
	public int wall_direction = 0;

	/// <summary>
	/// <c>true</c> if the player is currently facing right. <c>false</c> if the player 
	/// is facing left.
	/// </summary>
	bool faces_right = true;
	float bobble_time = 0.0f;
	/// <summary>
	/// <c>true</c> if the player sword can currently move.
	/// </summary>
	bool sword_can_move;
	/// <summary>
	/// <c>true</c> if the player arms can currently move.
	/// </summary>
	bool arms_can_move;
	/// <summary>
	/// Number of frames the attack button has been pressed.
	/// </summary>
	int attack_button_frames = 0;

	/// <summary>
	/// Current state of the player.
	/// </summary>
	PlayerStateId state = PlayerStateId.IDLE;
	/// <summary>
	/// Current collision boundary mode of the player.
	/// </summary>
	CollisionMode collision_mode = CollisionMode.STANDING;

	/// <summary>
	/// States the player can be in.
	/// <para>Maps a state's ID (<see cref="PlayerStateId"/>) to the actual state.</para>
	/// </summary>
	Dictionary<PlayerStateId, IPlayerState> registered_states = new Dictionary<PlayerStateId, IPlayerState>();

	/// <summary>
	/// Reference to $Sprites/ArmPosition for easy access.
	/// </summary>
	ArmPosition arm_position = new ArmPosition();
	Sprite2D arm2 = new Sprite2D();

	[Signal]
	public delegate void throw_swordEventHandler(Sword sword, Vector2 from, Vector2 direction, float rotation);

	public override void _Ready()
	{
		base._Ready();

		this.controls = new PlayerControls
		{
			up = "player_1_up",
			down = "player_1_down",
			right = "player_1_right",
			left = "player_1_left",
			attack = "player_1_attack",
			jump = "player_1_jump",
		};
		this.sword_scene = ResourceLoader.Load<PackedScene>("res://Sword/Sword.tscn");
		this.arm_position = this.GetNode<ArmPosition>("%ArmPosition");
		this.arm2 = this.GetNode<Sprite2D>("%Arm2");
		this.registered_states[PlayerStateId.IDLE] = new PlayerStateIdle();
		this.registered_states[PlayerStateId.RUNNING] = new PlayerStateRunning();
		this.registered_states[PlayerStateId.JUMPING] = new PlayerStateJumping();
		this.registered_states[PlayerStateId.FALLING] = new PlayerStateFalling();
		this.registered_states[PlayerStateId.CRAWLING] = new PlayerStateCrawling();
		this.registered_states[PlayerStateId.SLIDING] = new PlayerStateSliding();
		this.registered_states[PlayerStateId.SLIDE_CRAWLING] = new PlayerStateSlideCrawling();
		this.registered_states[PlayerStateId.ON_WALL] = new PlayerStateOnWall();
		this.registered_states[PlayerStateId.ON_WALL_FALLING] = new PlayerStateOnWallFalling();
		this.registered_states[PlayerStateId.LAUNCHING_SWORD] = new LaunchingSword();
		this.GetNode<AnimatedSprite2D>("%BodyAnimation").SpriteFrames.GetAnimationSpeed("idle");
		this.state = (new PlayerStateIdle()).NextState(this);
		this.ApplyState();
		this.TakeSword(this.sword_scene.Instantiate<Sword>());
	}

	public void CameraFollowPlayer(Node custom_viewport)
	{
		var camera = GetNode<Camera2D>("Camera2D");
		camera.MakeCurrent();
		camera.CustomViewport = custom_viewport;
	}

	public override void _PhysicsProcess(double delta_d)
	{
		base._PhysicsProcess(delta_d);
		var delta = (float)delta_d;

		var acceleration = this.registered_states[this.state].GetAcceleration(delta, this);
		this.Velocity += acceleration * delta;

		this.MoveArm(delta);
		var attacking = Input.IsActionJustPressed(this.controls.attack);
		if (Input.IsActionPressed(this.controls.attack) && Mathf.Abs(this.Velocity.X) >= RUNNING_THRESHOLD)
			this.attack_button_frames += 1;
		else
			this.attack_button_frames = 0;

		if (this.attack_button_frames == 10)
		{
			if (!this.ThrowSword(0.0f))
				this.TakeSword(this.sword_scene.Instantiate<Sword>());
		}
		else if (Input.IsActionJustPressed(this.controls.attack))
		{
			// TODO: make an attack !

		}

		this.speed_before_move_and_slide = this.Velocity;
		if (this.state == PlayerStateId.ON_WALL || this.state == PlayerStateId.ON_WALL_FALLING)
			this.wall_sucking_speed = -this.wall_direction * WALL_SUCKING_STRENGTH;
		else
			this.wall_sucking_speed = 0.0f;

		this.Velocity += new Vector2(this.wall_sucking_speed, 0.0f);
		this.MoveAndSlide();

		var old_state = this.state;
		this.state = this.registered_states[this.state].NextState(this);
		var state_kind = GetStateKind(this.state);

		if (state_kind == StateKind.STANDING || state_kind == StateKind.CRAWLING_OR_SLIDING)
		{
			this.on_wall_counts = 0;
		}

		if (this.state != old_state && this.state == PlayerStateId.IDLE)
			this.bobble_time = BOBBLE_ANIMATION_START;
		if (this.state != old_state && this.state == PlayerStateId.ON_WALL)
			this.GrabWall(this.Velocity.X - this.speed_before_move_and_slide.X);
		this.ApplyState();

		this.OrientationAndBobbles(delta);
	}

	void MoveArm(float delta)
	{
		var direction = Input.GetAxis(this.controls.up, this.controls.down);
		if (this.arms_can_move || this.sword_can_move)
		{
			this.arm_position.RotateArm(delta, direction);
		}
		else
			this.arm_position.Rotation = 0.0f;
		this.arm2.Rotation = this.arm_position.Rotation;
	}

	void GrabWall(float which_wall)
	{
		this.on_wall_counts += 1;
		if (which_wall > 0)
			this.wall_direction = 1;
		else
			this.wall_direction = -1;
		if (this.on_wall_counts >= 4)
		{
			if (this.on_wall_counts == 4)
			{
				// TODO: Little exclamation point !
			}
			this.state = PlayerStateId.ON_WALL_FALLING;
			return;
		}
		this.Velocity = this.Velocity with { Y = 0.0f };
	}

	// Change collisions, animations, show relevant parts of the character,
	// update friction and movement strength.
	void ApplyState()
	{
		var sword = this.arm_position.sword;
		var animation = this.GetNode<AnimatedSprite2D>("%BodyAnimation");

		var horizontal_movement = Mathf.Abs(this.Velocity.X);
		this.ground_friction = Physics.GROUND_FRICTION;

		switch (this.state)
		{
			case PlayerStateId.IDLE:
			case PlayerStateId.RUNNING:
			case PlayerStateId.JUMPING:
			case PlayerStateId.FALLING:
				if (horizontal_movement >= CHANGE_DIRECTION_THRESHOLD || (
						this.Velocity.X == 0
						&& Mathf.Abs(this.speed_before_move_and_slide.X) >= CHANGE_DIRECTION_THRESHOLD))
					this.faces_right = this.speed_before_move_and_slide.X > 0;
				break;
			case PlayerStateId.CRAWLING:
			case PlayerStateId.SLIDE_CRAWLING:
				break;
			case PlayerStateId.SLIDING:
				this.ground_friction = Physics.SLIDE_FRICTION;
				break;
			case PlayerStateId.ON_WALL:
			case PlayerStateId.ON_WALL_FALLING:
				this.faces_right = this.wall_direction == 1;
				break;
		}

		var state_properties = this.registered_states[this.state].state_properties;

		this.SetCollisionMode(state_properties.collision_mode);

		if (sword != null && state_properties.animation_sword != null)
			animation.Animation = state_properties.animation_sword;
		else
			animation.Animation = state_properties.animation;

		var current_frame = animation.Frame;

		if (state_properties.can_stop_if_no_movement && horizontal_movement < CRAWL_MOVE_THRESHOLD)
			animation.Stop();
		else
			animation.Play();
		if (state_properties.sword_positions_per_frame.Length > current_frame)
		{
			var (position, angle) = state_properties.sword_positions_per_frame[current_frame];
			if (state_properties.arms_can_move)
			{
				this.arm_position.Position = new Vector2(1, 0);
				if (sword != null)
					sword.Position = position;
			}
			else
			{
				this.arm_position.Position = position;
				if (sword != null)
					sword.Position = new Vector2(0, 0);
			}
			if (sword != null)
				sword.RotationDegrees = angle;
			this.sword_can_move = state_properties.can_move_sword;
		}
		else
			GD.PrintErr($"Unknown frame index {current_frame} for animation {state_properties.animation}");
		if (state_properties.arms_can_move)
		{
			this.arm_position.GetNode<CanvasItem>("Arm1").Show();
			if (this.arm_position.sword == null)
				this.arm2.Show();
			else
				this.arm2.Hide();
		}
		else
		{
			this.arm_position.GetNode<CanvasItem>("Arm1").Hide();
			this.arm2.Hide();
		}
		this.arms_can_move = state_properties.arms_can_move;
	}

	/// <summary>
	/// Set the <c>CollisionMode</c>.
	/// <para>This changes the collision shape.</para>
	/// </summary>
	void SetCollisionMode(CollisionMode mode)
	{
		var collision = GetNode<CollisionShape2D>("Collision");
		var new_shape = new RectangleShape2D();

		this.collision_mode = mode;
		if (mode == CollisionMode.STANDING)
		{
			new_shape.Size = new Vector2(4, 10);
			collision.Position = new Vector2(0, 3);
		}
		else
		{
			new_shape.Size = new Vector2(8, 8);
			collision.Position = new Vector2(0, 4);
		}
		collision.Shape = new_shape;
	}

	private static (float, float) GetCollisionBoxY(CollisionShape2D collision)
	{
		var shape = collision.Shape;
		var rect_shape = (RectangleShape2D)shape;
		return (collision.Position.Y, rect_shape.Size.Y);
	}

	// true if the player can stand without bonking their head against a ceiling.
	public bool CanStand()
	{
		if (this.collision_mode == CollisionMode.CRAWLING)
		{
			var collision = this.GetNode<CollisionShape2D>("Collision");
			var old_mode = this.collision_mode;
			this.SetCollisionMode(CollisionMode.STANDING);
			var (standing_y, standing_half) = GetCollisionBoxY(collision);
			this.SetCollisionMode(old_mode);
			var (crawling_y, crawling_half) = GetCollisionBoxY(collision);
			var dy = (standing_y - standing_half) - (crawling_y - crawling_half);
			var lower_dy = (standing_y + standing_half) - (crawling_y + crawling_half);
			//assert(lower_dy == 0) // why ?
			var stand_collision = this.TestMove(this.Transform, new Vector2(0, dy));
			return !stand_collision;
		}
		else
			return true;
	}

	// Change the sprites orientation depending on where we are facing.
	// Also make the character 'bouncy' when idle.
	void OrientationAndBobbles(float delta)
	{
		var sprites = GetNode<Node2D>("Sprites");

		this.SetOrientation();
		if (this.state == PlayerStateId.IDLE)
		{
			this.bobble_time += delta * Mathf.Pi * BOBBLE_SPEED;
			var bobble_position = new Vector2(
				Mathf.Cos(this.bobble_time),
				-Mathf.Abs(Mathf.Sin(this.bobble_time)));
			if (!this.faces_right)
				bobble_position.X = -bobble_position.X;
			sprites.Position = bobble_position;
		}
		else
			sprites.Position = new Vector2(0, 0);
	}

	void SetOrientation()
	{
		float sprites_scale;
		if (this.faces_right)
			sprites_scale = 1;
		else
			sprites_scale = -1;
		this.Scale = new Vector2(sprites_scale, 1);
		this.Rotation = 0;
	}

	void TakeSword(Sword sword)
	{
		this.running_strength = RUNNING_WITH_SWORD_STRENGTH;
		this.arm_position.TakeSword(sword);
	}

	// return `true` if the player was holding a sword
	bool ThrowSword(float rotation)
	{
		this.running_strength = RUNNING_WITHOUT_SWORD_STRENGTH;
		if (this.arm_position.ReleaseSword() is Sword sword)
		{
			var direction = new Vector2(1, 0);
			direction = direction.Rotated(this.arm_position.Rotation);
			direction = direction.Rotated(Mathf.DegToRad(-7.0f));
			if (!this.faces_right)
			{
				// direction = direction.Rotated(Mathf.Deg2Rad(10.0f));
				direction.X = -direction.X;

			}
			GD.Print("emmiting signal...");
			this.EmitSignal(
				"throw_sword",
				sword,
				this.GlobalPosition + this.arm_position.Position,
				direction * THROW_SWORD_STRENGTH, rotation);
			return true;
		}
		else
			return false;
	}
}
