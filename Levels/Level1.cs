using Godot;
using System.Collections.Generic;

public partial class Level1 : Node2D
{
	int number_of_swords = 0;
	List<Sword> swords_on_terrain = new List<Sword>();

	public override void _Ready()
	{
		base._Ready();

		float? limit_left = null;
		float? limit_right = null;
		float? limit_top = null;
		float? limit_bottom = null;

		var terrain = this.GetNode<TileMap>("Terrain");
		var cell_size = terrain.CellQuadrantSize;
		for (int i = 0; i < terrain.GetLayersCount(); i++)
		{
			foreach (var cell in terrain.GetUsedCells(i))
			{
				var position = (Vector2)cell * cell_size;
				if (limit_left == null || position.X < limit_left)
					limit_left = position.X;
				if (limit_right == null || position.X + cell_size - 1 > limit_right)
					limit_right = position.X + cell_size - 1;
				if (limit_top == null || position.Y < limit_top)
					limit_top = position.Y;
				if (limit_bottom == null || position.Y + cell_size > limit_bottom)
					limit_bottom = position.Y + cell_size;
			}
		}

		if (limit_left == null || limit_right == null || limit_top == null || limit_bottom == null)
			throw new System.Exception("no tiles set");

		foreach (var child in this.GetChildren())
		{
			if (child is Player player)
			{
				this.number_of_swords += 1;
				player.CameraFollowPlayer(this.GetNode(".."));
				var camera = player.GetNode<Camera2D>("Camera2D");
				camera.LimitLeft = (int)limit_left;
				camera.LimitTop = (int)limit_top;
				camera.LimitRight = (int)limit_right;
				camera.LimitBottom = (int)limit_bottom;
			}
		}
	}

	private void _on_Player_throw_sword(Sword sword, Vector2 hand_position, Vector2 direction, float rotation)
	{
		GD.Print("signal received");
		this.swords_on_terrain.Add(sword);
		this.AddChild(sword);
		sword.Position = hand_position + 20.0f * direction.Normalized();
		sword.ThrowInDirection(direction);
		sword.ApplyRotation(rotation);
		sword.Show();
	}
}
