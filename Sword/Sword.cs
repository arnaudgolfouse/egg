using Godot;
using System.Collections.Generic;

public partial class Sword : CharacterBody2D
{
	private bool _is_held;
	private bool _moving;

	// In kg
	private const float WEIGHT = 20.0f;
	private Vector2 center_of_mass;
	private Vector2 linear_velocity = new Vector2(0, 0);
	const float MINIMUM_LINEAR_VELOCITY = 25.0f;
	private float angular_velocity = 0.0f;
	const float LINEAR_VELOCITY_DAMP_WHILE_ROTATING = 1.0f;
	const float LINEAR_VELOCITY_DAMP_COLLISION = 1.4f;
	const float ANGULAR_VELOCITY_DAMP_WHILE_ROTATING = 1.3f;

	public bool IsHeld
	{
		get { return _is_held; }
		set
		{
			var sprite = GetNode<Sprite2D>("Sprite2D");
			var collision = GetNode<CollisionShape2D>("Collision");

			this._is_held = value;
			if (this._is_held)
			{
				sprite.Position = new Vector2(8, 0);
				sprite.Frame = 1;
				collision.Position = new Vector2(7.5f, 0.5f);
				collision.Disabled = true;
				this.CollisionLayer = Physics.DAMAGE_LAYER;
				this.CollisionMask = 0;
			}
			else
			{
				sprite.Position = new Vector2(-0.5f, 0.5f);
				sprite.Frame = 0;
				collision.Position = new Vector2(0, 0);
				collision.Disabled = false;
				this.CollisionMask = Physics.TERRAIN_LAYER;
			}
		}
	}

	public bool Moving
	{
		get { return this._moving; }
		set
		{
			if (this._is_held) return;
			this._moving = value;
			if (value)
			{
				this.CollisionLayer = Physics.DAMAGE_LAYER;
			}
			else
			{
				this.CollisionLayer = 0;
			}
		}
	}

	public override void _Ready()
	{
		base._Ready();
		this.center_of_mass = this.GetNode<CollisionShape2D>("Collision").Position;
		this.CollisionMask = Physics.TERRAIN_LAYER;
		this.IsHeld = false;
		this.Moving = true;
	}

	public void ThrowInDirection(Vector2 direction)
	{
		this.Moving = true;
		this.angular_velocity = 0.0f;
		this.linear_velocity = direction;
	}

	/// <summary>
	/// Apply a rotation, in rad.s⁻¹
	/// </summary>
	public void ApplyRotation(float rotation)
	{
		this.angular_velocity = rotation;
	}

	public override void _PhysicsProcess(double delta_d)
	{
		var delta = (float)delta_d;
		if (this.IsHeld || !this.Moving) return;
		if (this.angular_velocity == 0.0f)
		{
			this.Rotation = this.linear_velocity.Angle();
		}
		this.linear_velocity += delta * this.GetAcceleration(delta);
		this.Rotation += delta * this.angular_velocity;
		var collisition = this.MoveAndCollide(delta * this.linear_velocity);
		if (collisition == null)
			return;
		// collision occured
		this.linear_velocity /= LINEAR_VELOCITY_DAMP_COLLISION;
		if (collisition.GetNormal() == new Vector2(0, -1) && this.linear_velocity.Length() <= MINIMUM_LINEAR_VELOCITY)
		{
			this.linear_velocity = new Vector2(0, 0);
			if ((this.Rotation >= 0 && this.Rotation <= Mathf.Pi / 2) || (this.Rotation <= 0 && this.Rotation >= -Mathf.Pi / 2))
			{
				this.Rotation = 0;
			}
			else
			{
				this.Rotation = Mathf.Pi;
			}
			this.Position = this.Position.Project(collisition.GetPosition() * collisition.GetPosition().Dot(collisition.GetNormal()));
			this.Moving = false;
			return;
		}
		var point = this.ToLocal(collisition.GetPosition());
		var normal = collisition.GetNormal();
		var plane = normal.Rotated(Mathf.Pi / 2);
		var remainder = collisition.GetRemainder().Reflect(plane);
		this.linear_velocity = this.linear_velocity.Reflect(plane);
		this.linear_velocity -= 5.0f * normal;
		if (this.angular_velocity != 0.0)
		{
			this.angular_velocity = -this.angular_velocity;
			this.angular_velocity /= ANGULAR_VELOCITY_DAMP_WHILE_ROTATING;
			// Convert the lost angular velocity into linear velocity.
			this.linear_velocity += (Mathf.Abs(this.angular_velocity) * (ANGULAR_VELOCITY_DAMP_WHILE_ROTATING - 1.0f)) * 2.0f * this.linear_velocity.Normalized();
			this.linear_velocity /= LINEAR_VELOCITY_DAMP_WHILE_ROTATING;
		}
	}

	private Vector2 GetAcceleration(float delta)
	{
		return new Vector2(0, WEIGHT * Physics.GRAVITY) - this.linear_velocity * Physics.AIR_FRICTION;
	}

	private float GetAngularInertia()
	{
		var transparent_color = new Color(0, 0, 0, 0);
		var sprite = this.GetNode<Sprite2D>("Sprite2D");
		var sprite_image = sprite.Texture.GetImage();
		var frame_height = sprite_image.GetHeight() / sprite.Vframes;
		var frame_width = sprite_image.GetWidth() / sprite.Hframes;
		var frame_coords = sprite.FrameCoords;
		var start_y = (int)frame_coords.Y * frame_height;
		var end_y = ((int)frame_coords.Y + 1) * frame_height;
		var start_x = (int)frame_coords.X * frame_width;
		var end_x = ((int)frame_coords.X + 1) * frame_width;
		var non_empty_pixels = new List<Vector2>();
		for (int y = start_y; y < end_y; y++)
		{
			for (int x = start_x; x < end_x; x++)
			{
				var v = new Vector2I(x, y);
				var color = sprite_image.GetPixelv(v);
				if (color.A != 0)
				{
					non_empty_pixels.Add(v);
				}
			}
		}
		var inertia = 0.0f;
		var weigth_per_pixel = WEIGHT / non_empty_pixels.Count;
		foreach (var v in non_empty_pixels)
		{
			var distance = v.DistanceTo(this.center_of_mass);
			// Assuming each pixel weigths the same
			inertia += weigth_per_pixel * distance * distance;
		}
		return inertia;
	}
}
