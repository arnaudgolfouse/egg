using Godot;

public partial class Physics : Node
{
	public const float GRAVITY = 9.8f;

	// Friction that is always applied
	public const float AIR_FRICTION = 1.0f;
	// Friction when against a wall
	public const float WALL_FRICTION = 20.0f;
	// Friction when on the ground. Used for walking, crawling, etc...
	public const float GROUND_FRICTION = 7.0f;
	// Friction when sliding
	public const float SLIDE_FRICTION = 1.5f;

	// Collision layer for solid object: ground and everything that interacts with it.
	public const int TERRAIN_LAYER = 1 << 0;
	// Collision layer for damaging object: sword, fists, etc.
	public const int DAMAGE_LAYER = 1 << 1;
}
